//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var arrayItems = [Int] ()
arrayItems.append(1)
arrayItems.append(2)
var arrayItems1: [Int] = [Int] ()

arrayItems1 = arrayItems
arrayItems1.append(3)
arrayItems
arrayItems = []
