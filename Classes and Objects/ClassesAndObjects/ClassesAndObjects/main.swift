//
//  main.swift
//  ClassesAndObjects
//
//  Created by Kunal Shah on 5/20/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.
//

import Foundation
let myCar = Car("Grey", 14)
print(myCar.type)
print(myCar.numberOfSeats)

let car = Car()
print("car object color \(car.colour)")

car.drive()

let mySelfDrivingCar = SelfDrivingCar("s", 6)
mySelfDrivingCar.drive()
print(mySelfDrivingCar.colour)
print(mySelfDrivingCar.numberOfSeats)
mySelfDrivingCar.destination = "Hareshwar Apartments"
mySelfDrivingCar.drive()

let self2 = Self2("self", 2)


print(self2.colour)
print(self2.numberOfSeats)
