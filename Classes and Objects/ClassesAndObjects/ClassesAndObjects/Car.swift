//
//  Car.swift
//  ClassesAndObjects
//
//  Created by Kunal Shah on 5/20/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.
//

import Foundation
enum CarType {
    case Sedan
    case Coupe
    case HatchBack
}
class Car {
    var colour: String = "Black"
    var numberOfSeats: Int = 5
    var type : CarType = .Coupe
    init () {
        
    }
    convenience init (_ color: String,_ noOfSeats: Int) {
        self.init()
        colour = color
        numberOfSeats = noOfSeats
    }
    
    func drive () {
        print("Car is moving ")
    }
    
}
