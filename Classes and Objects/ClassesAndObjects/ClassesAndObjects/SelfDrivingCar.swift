//
//  SelfDrivingCar.swift
//  ClassesAndObjects
//
//  Created by Kunal Shah on 5/20/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.
//

import Foundation

class SelfDrivingCar : Car {
    var destination: String?
    
    override func drive() {
        super.drive()
        if destination != nil {
        print("Driving without driver towards " + destination!)
        }
        else {
            print("No destination entered")
        }
    }
}
