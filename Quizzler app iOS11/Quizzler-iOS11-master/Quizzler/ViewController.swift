//
//  ViewController.swift
//  Quizzler
//  Created by Shripal Shah on 5/19/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.

import UIKit

class ViewController: UIViewController {
    
    //Place your instance variables here
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    
    let questionBank = QuestionBank()
    var size = 0
    var questionNumber = 0
    let answer = [true, false]
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        size = questionBank.list.count
        questionLabel.text = questionBank.list[questionNumber].questionText
        print(questionBank.list.count)
        progressLabel.text = String(questionNumber + 1)
    }


    @IBAction func answerPressed(_ sender: AnyObject) {
        checkAnswer(sender)
    }
    
    
    func updateUI() {
        questionLabel.text = questionBank.list[questionNumber].questionText
        progressLabel.text = String (questionNumber + 1)
    }
    

    func nextQuestion() {
        print(questionNumber)
        if questionNumber < size-1{
            questionNumber = questionNumber + 1
            updateUI()
        }
        else {
           startOver()
        }
    }
    
    
    func checkAnswer(_ sender : AnyObject) {
        if questionBank.list[questionNumber].answer == answer[sender.tag - 1] {
            score = score + 1
            scoreLabel.text = String(score)
            nextQuestion()
            
        }
        else {
            nextQuestion()
            
        }
    }
    
    
    func startOver() {
       questionNumber = 0
        score = 0
        updateUI()
    }
    

    
}
