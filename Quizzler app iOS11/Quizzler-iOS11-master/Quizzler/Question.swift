//
//  Question.swift
//  Quizzler
//
//  Created by Shripal Shah on 5/17/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.
//
// library Foundation its lighter than UIKit also could go with import swift but ios- either of on is required

import Foundation

class Question {
    // create peoperties: the variables that are associated with the class
    let questionText : String
    let answer : Bool
    
    init (text: String, correctAnswer: Bool) {
        questionText = text
        answer = correctAnswer
    }
}



