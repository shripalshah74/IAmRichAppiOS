//
//  ViewController.swift
//  Positioning UI Element Programmatically
//
//  Created by Kunal Shah on 5/20/18.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let square = UIView (frame: CGRect(x: 0 , y: 0, width: self.view.frame.width * 0.95, height : self.view.frame.height
        * 0.95))
        square.backgroundColor = UIColor.black
        self.view.addSubview(square)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

