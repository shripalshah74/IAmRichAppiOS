//: Playground - noun: a place where people can play

import UIKit

var myAge = 27

myAge = 26

let myName : String = "Shripal"
let myAgeInTenYears = myAge + 10
let myDetails = "\(myName), \(myAge)"

let wholeNumbers : Int = 12
let text : String = "abc"

let boolean : Bool = true
let floatingPointNumber : Float = 3.122

let double : Double = 3.14159265359121


