//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Shripal Shah on 5/18/18.
//  Copyright © 2018 Shripal Shah. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ballImageView: UIImageView!
    var randomNumber = 0
    let ballImage = ["ball1","ball2","ball3","ball4","ball5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        update()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func askQuestion(_ sender: UIButton) {
        update()
    }
    func update() {
        randomNumber = Int(arc4random_uniform(5))
        ballImageView.image = UIImage (named: ballImage[randomNumber])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        update()
    }


}

